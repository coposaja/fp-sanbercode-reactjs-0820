import React, { createContext, useState } from 'react';

export const AuthContext = createContext();

export const AuthProvider = (props) => {
  const currentUser = JSON.parse(localStorage.getItem('auth'));
  const initUser = currentUser ? currentUser : null;
  const [user, setUser] = useState(initUser);

  return (
    <AuthContext.Provider value={[user, setUser]}>
      {props.children}
    </AuthContext.Provider>
  )
}
