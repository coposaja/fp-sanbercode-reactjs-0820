import { Button, Card, CardContent, Grid, TextField } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { customAxios } from '../../utils';

import './MovieForm.css';

const MovieForm = (props) => {
  const [id,] = useState(props.match.params.id);
  const [title, setTitle] = useState('');
  const [genre, setGenre] = useState('');
  const [rating, setRating] = useState('');
  const [duration, setDuration] = useState('');
  const [year, setYear] = useState('');
  const [imageUrl, setImageUrl] = useState('');
  const [description, setDescription] = useState('');
  const [review, setReview] = useState('');
  const [errMessage, setErrMessage] = useState({});

  useEffect(() => {
    if (id > 0) {
      customAxios.get(`data-movie/${id}`)
        .then((res) => {
          const movie = res.data;
          setTitle(movie.title)
          setGenre(movie.genre)
          setRating(movie.rating)
          setDuration(movie.duration)
          setYear(movie.year)
          setImageUrl(movie.image_url)
          setDescription(movie.description)
          setReview(movie.review)
        })
        .catch((err) => console.log(err));
    }
  }, [id])

  const submitHandler = (e) => {
    e.preventDefault();
    if (!validateInput()) return;

    const movie = {
      title: title,
      genre: genre,
      rating: rating,
      duration: duration,
      year: year,
      image_url: imageUrl,
      description: description,
      review: review,
    }

    if (id > 0) {
      customAxios.put(`data-movie/${id}`, movie)
        .then((res) => {
          props.history.push(`/movies/${res.data.id}`)
        })
        .catch((err) => console.log(err));
    } else {
      customAxios.post('data-movie', movie)
        .then((res) => {
          props.history.push(`/movies/${res.data.id}`)
        })
        .catch((err) => console.log(err));
    }
  }

  const validateInput = () => {
    let error = {};
    let flag = true;
    if (!title) {
      error.title = "Please input movie title";
      flag = false;
    }

    if (!genre) {
      error.genre = "Please input movie genre";
      flag = false;
    }

    if (!rating) {
      error.rating = "Please input movie rating";
      flag = false;
    }
    else if (isNaN(rating)) {
      error.rating = "Please input valid movie rating";
      flag = false;
    }

    if (!duration) {
      error.duration = "Please input movie duration";
      flag = false;
    }
    else if (isNaN(duration)) {
      error.duration = "Please input valid movie duration";
      flag = false;
    }

    if (!year) {
      error.year = "Please input movie year";
      flag = false;
    }
    else if (isNaN(year)) {
      error.year = "Please input valid movie year";
      flag = false;
    }

    if (!imageUrl) {
      error.imageUrl = "Please input movie image url";
      flag = false;
    }

    if (!description) {
      error.description = "Please input movie description";
      flag = false;
    }

    if (!review) {
      error.review = "Please input movie review";
      flag = false;
    }

    setErrMessage(error);
    return flag;
  }

  return (
    <div className="movieform">
      <Card variant="outlined">
        <CardContent style={{ display: 'flex' }}>
          <img
            style={{ height: '500px', width: '330px', objectFit: 'contain' }}
            src={imageUrl}
            onError={(e) => e.target.src = `https://images-na.ssl-images-amazon.com/images/I/21u5Kw1n+IL._SX331_BO1,204,203,200_.jpg`}
            alt="Moive Poster"
          />
          <form className="movieform--form" onSubmit={(e) => submitHandler(e)}>
            <Grid container justify="center" spacing={2}>
              <Grid item xs={6}>
                <TextField
                  type="text"
                  label="Title"
                  variant="outlined"
                  fullWidth
                  value={title}
                  onChange={(e) => setTitle(e.target.value)}
                  error={errMessage.title ? true : false}
                  helperText={errMessage.title}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  type="text"
                  label="Genre"
                  variant="outlined"
                  fullWidth
                  value={genre}
                  onChange={(e) => setGenre(e.target.value)}
                  error={errMessage.genre ? true : false}
                  helperText={errMessage.genre}
                />
              </Grid>
              <Grid item xs={4}>
                <TextField
                  type="text"
                  label="Rating"
                  variant="outlined"
                  fullWidth
                  value={rating}
                  onChange={(e) => setRating(e.target.value)}
                  error={errMessage.rating ? true : false}
                  helperText={errMessage.rating}
                />
              </Grid>
              <Grid item xs={4}>
                <TextField
                  type="text"
                  label="Duration"
                  variant="outlined"
                  fullWidth
                  value={duration}
                  onChange={(e) => setDuration(e.target.value)}
                  error={errMessage.duration ? true : false}
                  helperText={errMessage.duration}
                />
              </Grid>
              <Grid item xs={4}>
                <TextField
                  type="text"
                  label="Year"
                  variant="outlined"
                  fullWidth
                  value={year}
                  onChange={(e) => setYear(e.target.value)}
                  error={errMessage.year ? true : false}
                  helperText={errMessage.year}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  type="text"
                  label="Image Url"
                  variant="outlined"
                  fullWidth
                  value={imageUrl}
                  onChange={(e) => setImageUrl(e.target.value)}
                  error={errMessage.imageUrl ? true : false}
                  helperText={errMessage.imageUrl}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  type="text"
                  label="Description"
                  variant="outlined"
                  fullWidth
                  value={description}
                  onChange={(e) => setDescription(e.target.value)}
                  error={errMessage.description ? true : false}
                  helperText={errMessage.description}
                  multiline
                  rows={4}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  type="text"
                  label="Review"
                  variant="outlined"
                  fullWidth
                  value={review}
                  onChange={(e) => setReview(e.target.value)}
                  error={errMessage.review ? true : false}
                  helperText={errMessage.review}
                  multiline
                  rows={4}
                />
              </Grid>
            </Grid>
            <Button
              variant="contained"
              color="primary"
              size="large"
              type="submit"
              fullWidth
            >
              Submit
            </Button>
          </form>
        </CardContent>
      </Card>
    </div>
  )
}

export default MovieForm
