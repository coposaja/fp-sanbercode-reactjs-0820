import {
  Button,
  Checkbox,
  Divider,
  Drawer,
  List,
  ListItem,
  MenuItem,
  Table,
  TableBody,
  TableCell,
  TablePagination,
  TableRow,
  TextField,
} from '@material-ui/core';
import React, { useEffect, useState } from 'react';

import { customAxios } from '../../utils';
import EnhancedTableHead from '../../components/Table/TableHead';
import EnhancedTableToolbar from '../../components/Table/TableToolbar';
import './MovieList.css';

const MovieList = (props) => {
  const [movies, setMovies] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [order, setOrder] = useState('asc');
  const [orderBy, setOrderBy] = useState('title');
  const [selected, setSelected] = useState([]);
  const [fetch, setFetch] = useState(false);
  const [filter, setFilter] = useState({ search: '', genre: 'All', rating: 'All', year: 'All' });
  const [filterOption, setFilterOption] = useState({});
  const [filteredMovies, setFilteredMovies] = useState([]);
  const emptyRows = rowsPerPage - Math.min(rowsPerPage, movies.length - page * rowsPerPage);
  const headCells = [
    { id: 'title', disablePadding: false, label: 'Title' },
    { id: 'description', disablePadding: false, label: 'Description' },
    { id: 'genre', disablePadding: false, label: 'Genre' },
    { id: 'review', disablePadding: false, label: 'Review' },
    { id: 'rating', disablePadding: false, label: 'Rating' },
    { id: 'duration', disablePadding: false, label: 'Duration' },
    { id: 'year', disablePadding: false, label: 'Year' },
  ];

  useEffect(() => {
    customAxios.get('data-movie')
      .then((res) => {
        setMovies(res.data);
        initFilterOption(res.data);
        setFilteredMovies(res.data);
      })
      .catch((err) => console.log(err));

    const initFilterOption = (data) => {
      const genres = [];
      const ratings = [];
      const years = [];

      data.map((movie) => {
        genres.push(movie.genre);
        ratings.push(movie.rating);
        years.push(movie.year);
        return movie;
      });

      setFilterOption({
        genres: Array.from([...new Set(genres)]),
        ratings: Array.from([...new Set(ratings)]),
        years: Array.from([...new Set(years)])
      });
    }
  }, []);

  useEffect(() => {
    if (fetch) {
      customAxios.get('data-movie')
        .then((res) => {
          setMovies(res.data);
          setFetch(false);
          initFilterOption(res.data);
          setFilteredMovies(res.data);
        })
        .catch((err) => console.log(err));

      const initFilterOption = (data) => {
        const genres = [];
        const ratings = [];
        const years = [];

        data.map((movie) => {
          genres.push(movie.genre);
          ratings.push(movie.rating);
          years.push(movie.year);
          return movie;
        });

        setFilterOption({
          genres: Array.from([...new Set(genres)]),
          ratings: Array.from([...new Set(ratings)]),
          years: Array.from([...new Set(years)])
        });
      }
    }
  }, [fetch]);

  const getComparator = (order, orderBy) => {
    return order === 'desc'
      ? (a, b) => descendingComparator(a, b, orderBy)
      : (a, b) => -descendingComparator(a, b, orderBy);
  }

  const descendingComparator = (a, b, orderBy) => {
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }
    if (b[orderBy] > a[orderBy]) {
      return 1;
    }
    return 0;
  }

  const stableSort = (array, comparator) => {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
      const order = comparator(a[0], b[0]);
      if (order !== 0) return order;
      return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
  }

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = movies.map((movie) => movie.id);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const handleCheckboxClick = (event, id) => {
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    setSelected(newSelected);
  };

  const handleDelete = () => {
    selected.map((id) => {
      customAxios.delete(`data-movie/${id}`)
        .then((res) => {
          console.log(res);
          selected.pop();
          if (!selected.length) setFetch(true);
        })
        .catch((err) => console.log(err));
      return id;
    });
  }

  const handleEdit = () => {
    props.history.push(`/Movies/${selected[0]}`);
  }

  const handleAdd = () => {
    props.history.push(`/Movies/0`);
  }

  const handleFilter = () => {
    const { search, genre, rating, year } = filter;

    const filtered = movies
      .filter((movie) => movie.title.toLowerCase().includes(search ?? ''))
      .filter((movie) => movie.genre.includes(genre === 'All' ? '' : genre))
      .filter((movie) => movie.rating === (rating === 'All' ? movie.rating : rating))
      .filter((movie) => movie.year === (year === 'All' ? movie.year : year));

    setFilteredMovies(filtered);
  }

  const isSelected = (id) => selected.indexOf(id) !== -1;

  return (
    <div className="movielist">
      <Drawer
        variant="permanent"
        anchor="left"
        className="movielist--sidebar"
        classes={{
          paper: 'movielist--sidebar'
        }}
      >
        <Divider />
        <List>
          <ListItem>
            <TextField
              type="text"
              label="Search by title"
              variant="outlined"
              fullWidth
              value={filter.search}
              onChange={(e) => setFilter({ ...filter, search: e.target.value })}
            />
          </ListItem>
          <ListItem>
            <TextField
              select
              label="Genre"
              helperText="Filter by Genre"
              fullWidth
              value={filter.genre}
              onChange={(e) => setFilter({ ...filter, genre: e.target.value })}
            >
              <MenuItem key={'All'} value='All'>All</MenuItem>
              {filterOption.genres && filterOption.genres.map((option) => (
                <MenuItem key={option} value={option}>
                  {option}
                </MenuItem>
              ))}
            </TextField>
          </ListItem>
          <ListItem>
            <TextField
              select
              label="Rating"
              helperText="Filter by Rating"
              fullWidth
              value={filter.rating}
              onChange={(e) => setFilter({ ...filter, rating: e.target.value })}
            >
              <MenuItem key={'All'} value='All'>All</MenuItem>
              {filterOption.ratings && filterOption.ratings.map((option) => (
                <MenuItem key={option} value={option}>
                  {option}
                </MenuItem>
              ))}
            </TextField>
          </ListItem>
          <ListItem>
            <TextField
              select
              label="Year"
              helperText="Filter by Year"
              fullWidth
              value={filter.year}
              onChange={(e) => setFilter({ ...filter, year: e.target.value })}
            >
              <MenuItem key={'All'} value='All'>All</MenuItem>
              {filterOption.years && filterOption.years.map((option) => (
                <MenuItem key={option} value={option}>
                  {option}
                </MenuItem>
              ))}
            </TextField>
          </ListItem>
          <ListItem>
            <Button
              variant="contained"
              color="primary"
              size="large"
              fullWidth
              onClick={(e) => handleFilter()}
            >
              Apply Filter
            </Button>
          </ListItem>
        </List>
      </Drawer>
      <EnhancedTableToolbar
        selected={selected}
        handleDelete={handleDelete}
        handleEdit={handleEdit}
        handleAdd={handleAdd}
        tableType={"Movie"}
      />
      <Table stickyHeader className='movielist--table' aria-label="simple table">
        <EnhancedTableHead
          headCells={headCells}
          numSelected={selected.length}
          order={order}
          orderBy={orderBy}
          onSelectAllClick={handleSelectAllClick}
          onRequestSort={handleRequestSort}
          rowCount={movies.length}
        />
        <TableBody>
          {filteredMovies.length ? (
            <>
              {stableSort(filteredMovies, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((movie, index) => {
                  const isItemSelected = isSelected(movie.id);
                  const labelId = `enhanced-table-checkbox-${index}`;

                  return (
                    <TableRow
                      hover
                      role="checkbox"
                      aria-checked={isItemSelected}
                      tabIndex={-1}
                      key={movie.id}
                      selected={isItemSelected}
                    >
                      <TableCell padding="checkbox">
                        <Checkbox
                          checked={isItemSelected}
                          inputProps={{ 'aria-labelledby': labelId }}
                          onClick={(event) => handleCheckboxClick(event, movie.id)}
                        />
                      </TableCell>
                      <TableCell>{movie.title}</TableCell>
                      <TableCell>{movie.description}</TableCell>
                      <TableCell>{movie.genre}</TableCell>
                      <TableCell>{movie.review}</TableCell>
                      <TableCell>{movie.rating}</TableCell>
                      <TableCell>{movie.duration}</TableCell>
                      <TableCell>{movie.year}</TableCell>
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ emptyRows }}>
                  <TableCell colSpan={7} />
                </TableRow>
              )}
            </>
          ) : (
              <TableRow style={{ emptyRows }}>
                <TableCell colSpan={7} style={{ fontWeight: 'bold', textAlign: 'center' }}>No Data</TableCell>
              </TableRow>
            )}
        </TableBody>
      </Table>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={movies.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </div>
  )
}

export default MovieList;
