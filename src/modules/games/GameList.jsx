import {
  Button,
  Checkbox,
  Divider,
  Drawer,
  List,
  ListItem,
  MenuItem,
  Table,
  TableBody,
  TableCell,
  TablePagination,
  TableRow,
  TextField,
} from '@material-ui/core';
import React, { useEffect, useState } from 'react';

import { customAxios } from '../../utils';
import EnhancedTableHead from '../../components/Table/TableHead';
import EnhancedTableToolbar from '../../components/Table/TableToolbar';
import './GameList.css';

const GameList = (props) => {
  const [games, setGames] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [order, setOrder] = useState('asc');
  const [orderBy, setOrderBy] = useState('title');
  const [selected, setSelected] = useState([]);
  const [fetch, setFetch] = useState(false);
  const [filter, setFilter] = useState({ search: '', genre: 'All', release: 'All', platform: 'All' });
  const [filterOption, setFilterOption] = useState({});
  const [filteredGames, setFilteredGames] = useState([]);
  const emptyRows = rowsPerPage - Math.min(rowsPerPage, games.length - page * rowsPerPage);
  const headCells = [
    { id: 'name', disablePadding: false, label: 'Name' },
    { id: 'platform', disablePadding: false, label: 'Platform' },
    { id: 'genre', disablePadding: false, label: 'Genre' },
    { id: 'release', disablePadding: false, label: 'Release' },
    { id: 'multiplayer', disablePadding: false, label: 'Multiplayer' },
    { id: 'singlePlayer', disablePadding: false, label: 'Single Player' },
  ];

  useEffect(() => {
    customAxios.get('data-game')
      .then((res) => {
        setGames(res.data);
        initFilterOption(res.data);
        setFilteredGames(res.data);
      })
      .catch((err) => console.log(err));

    const initFilterOption = (data) => {
      const genres = [];
      const releases = [];
      const platforms = [];

      data.map((game) => {
        genres.push(game.genre);
        releases.push(game.release);
        platforms.push(game.platform);
        return game;
      });

      setFilterOption({
        genres: Array.from([...new Set(genres)]),
        releases: Array.from([...new Set(releases)]),
        platforms: Array.from([...new Set(platforms)])
      });
    }
  }, []);

  useEffect(() => {
    if (fetch) {
      customAxios.get('data-game')
        .then((res) => {
          setGames(res.data);
          setFetch(false);
          initFilterOption(res.data);
          setFilteredGames(res.data);
        })
        .catch((err) => console.log(err));

      const initFilterOption = (data) => {
        const genres = [];
        const releases = [];
        const platforms = [];

        data.map((game) => {
          genres.push(game.genre);
          releases.push(game.release);
          platforms.push(game.platform);
          return game;
        });

        setFilterOption({
          genres: Array.from([...new Set(genres)]),
          releases: Array.from([...new Set(releases)]),
          platforms: Array.from([...new Set(platforms)])
        });
      }
    }
  }, [fetch]);

  const getComparator = (order, orderBy) => {
    return order === 'desc'
      ? (a, b) => descendingComparator(a, b, orderBy)
      : (a, b) => -descendingComparator(a, b, orderBy);
  }

  const descendingComparator = (a, b, orderBy) => {
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }
    if (b[orderBy] > a[orderBy]) {
      return 1;
    }
    return 0;
  }

  const stableSort = (array, comparator) => {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
      const order = comparator(a[0], b[0]);
      if (order !== 0) return order;
      return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
  }

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = games.map((game) => game.id);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const handleCheckboxClick = (event, id) => {
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    setSelected(newSelected);
  };

  const handleDelete = () => {
    selected.map((id) => {
      customAxios.delete(`data-game/${id}`)
        .then((res) => {
          console.log(res);
          selected.pop();
          if (!selected.length) setFetch(true);
        })
        .catch((err) => console.log(err));
      return id;
    });
  }

  const handleEdit = () => {
    props.history.push(`/Games/${selected[0]}`);
  }

  const handleAdd = () => {
    props.history.push(`/Games/0`);
  }

  const handleFilter = () => {
    const { search, genre, platform, release } = filter;

    const filtered = games
      .filter((game) => game.name.toLowerCase().includes(search ?? ''))
      .filter((game) => game.genre.includes(genre === 'All' ? '' : genre))
      .filter((game) => game.platform.includes(platform === 'All' ? '' : platform))
      .filter((game) => game.release === (release === 'All' ? game.release : release));

    setFilteredGames(filtered);
  }

  const isSelected = (id) => selected.indexOf(id) !== -1;

  return (
    <div className="gamelist">
      <Drawer
        variant="permanent"
        anchor="left"
        className="gamelist--sidebar"
        classes={{
          paper: 'gamelist--sidebar'
        }}
      >
        <Divider />
        <List>
          <ListItem>
            <TextField
              type="text"
              label="Search by name"
              variant="outlined"
              fullWidth
              value={filter.search}
              onChange={(e) => setFilter({ ...filter, search: e.target.value })}
            />
          </ListItem>
          <ListItem>
            <TextField
              select
              label="Genre"
              helperText="Filter by Genre"
              fullWidth
              value={filter.genre}
              onChange={(e) => setFilter({ ...filter, genre: e.target.value })}
            >
              <MenuItem key={'All'} value='All'>All</MenuItem>
              {filterOption.genres && filterOption.genres.map((option) => (
                <MenuItem key={Math.random()} value={option}>
                  {option}
                </MenuItem>
              ))}
            </TextField>
          </ListItem>
          <ListItem>
            <TextField
              select
              label="Release"
              helperText="Filter by Release"
              fullWidth
              value={filter.release}
              onChange={(e) => setFilter({ ...filter, release: e.target.value })}
            >
              <MenuItem key={'All'} value='All'>All</MenuItem>
              {filterOption.releases && filterOption.releases.map((option) => (
                <MenuItem key={Math.random()} value={option}>
                  {option}
                </MenuItem>
              ))}
            </TextField>
          </ListItem>
          <ListItem>
            <TextField
              select
              label="Platform"
              helperText="Filter by Platform"
              fullWidth
              value={filter.platform}
              onChange={(e) => setFilter({ ...filter, platform: e.target.value })}
            >
              <MenuItem key={'All'} value='All'>All</MenuItem>
              {filterOption.platforms && filterOption.platforms.map((option) => (
                <MenuItem key={Math.random()} value={option}>
                  {option}
                </MenuItem>
              ))}
            </TextField>
          </ListItem>
          <ListItem>
            <Button
              variant="contained"
              color="primary"
              size="large"
              fullWidth
              onClick={(e) => handleFilter()}
            >
              Apply Filter
            </Button>
          </ListItem>
        </List>
      </Drawer>
      <EnhancedTableToolbar
        selected={selected}
        handleDelete={handleDelete}
        handleEdit={handleEdit}
        handleAdd={handleAdd}
        tableType={"Game"}
      />
      <Table stickyHeader className='gamelist--table' aria-label="simple table">
        <EnhancedTableHead
          headCells={headCells}
          numSelected={selected.length}
          order={order}
          orderBy={orderBy}
          onSelectAllClick={handleSelectAllClick}
          onRequestSort={handleRequestSort}
          rowCount={games.length}
        />
        <TableBody>
          {filteredGames.length ? (
            <>
              {stableSort(filteredGames, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((game, index) => {
                  const isItemSelected = isSelected(game.id);
                  const labelId = `enhanced-table-checkbox-${index}`;

                  return (
                    <TableRow
                      hover
                      role="checkbox"
                      aria-checked={isItemSelected}
                      tabIndex={-1}
                      key={game.id}
                      selected={isItemSelected}
                    >
                      <TableCell padding="checkbox">
                        <Checkbox
                          checked={isItemSelected}
                          inputProps={{ 'aria-labelledby': labelId }}
                          onClick={(event) => handleCheckboxClick(event, game.id)}
                        />
                      </TableCell>
                      <TableCell>{game.name}</TableCell>
                      <TableCell>{game.platform}</TableCell>
                      <TableCell>{game.genre}</TableCell>
                      <TableCell>{game.release}</TableCell>
                      <TableCell>{game.multiplayer}</TableCell>
                      <TableCell>{game.singlePlayer}</TableCell>
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ emptyRows }}>
                  <TableCell colSpan={7} />
                </TableRow>
              )}
            </>
          ) : (
              <TableRow style={{ emptyRows }}>
                <TableCell colSpan={7} style={{ fontWeight: 'bold', textAlign: 'center' }}>No Data</TableCell>
              </TableRow>
            )}
        </TableBody>
      </Table>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={games.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </div>
  )
}

export default GameList;
