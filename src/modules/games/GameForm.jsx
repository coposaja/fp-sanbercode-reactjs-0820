import { Button, Card, CardContent, Grid, TextField } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { customAxios } from '../../utils';

import './GameForm.css';

const GameForm = (props) => {
  const [id,] = useState(props.match.params.id);
  const [name, setName] = useState('');
  const [genre, setGenre] = useState('');
  const [release, setRelease] = useState('');
  const [singlePlayer, setSinglePlayer] = useState('');
  const [multiplayer, setMultiplayer] = useState('');
  const [imageUrl, setImageUrl] = useState('');
  const [platform, setPlatform] = useState('');
  const [errMessage, setErrMessage] = useState({});

  useEffect(() => {
    if (id > 0) {
      customAxios.get(`data-game/${id}`)
        .then((res) => {
          const game = res.data;
          setName(game.name)
          setGenre(game.genre)
          setRelease(game.release)
          setSinglePlayer(game.singlePlayer)
          setMultiplayer(game.multiplayer)
          setImageUrl(game.image_url)
          setPlatform(game.platform)
        })
        .catch((err) => console.log(err));
    }
  }, [id])

  const submitHandler = (e) => {
    e.preventDefault();
    if (!validateInput()) return;

    const game = {
      name: name,
      genre: genre,
      release: release,
      singlePlayer: singlePlayer,
      multiplayer: multiplayer,
      image_url: imageUrl,
      platform: platform,
    }

    if (id > 0) {
      customAxios.put(`data-game/${id}`, game)
        .then((res) => {
          props.history.push(`/games/${res.data.id}`)
        })
        .catch((err) => console.log(err));
    } else {
      customAxios.post('data-game', game)
        .then((res) => {
          props.history.push(`/games/${res.data.id}`)
        })
        .catch((err) => console.log(err));
    }
  }

  const validateInput = () => {
    let error = {};
    let flag = true;
    if (!name) {
      error.name = "Please input game name";
      flag = false;
    }

    if (!genre) {
      error.genre = "Please input game genre";
      flag = false;
    }

    if (!release) {
      error.release = "Please input game release";
      flag = false;
    }
    else if (isNaN(release)) {
      error.release = "Please input valid game release";
      flag = false;
    }

    if (!singlePlayer) {
      error.singlePlayer = "Please input if game is singlePlayer";
      flag = false;
    }
    else if (isNaN(singlePlayer)) {
      error.duration = "Please input valid game singlePlayer";
      flag = false;
    }

    if (!multiplayer) {
      error.multiplayer = "Please input if game is multiplayer";
      flag = false;
    }
    else if (isNaN(multiplayer)) {
      error.multiplayer = "Please input valid game player count";
      flag = false;
    }

    if (!imageUrl) {
      error.imageUrl = "Please input game image url";
      flag = false;
    }

    if (!platform) {
      error.description = "Please input game platform";
      flag = false;
    }

    setErrMessage(error);
    return flag;
  }

  return (
    <div className="gameform">
      <Card variant="outlined">
        <CardContent style={{ display: 'flex' }}>
          <img
            style={{ height: '500px', width: '330px', objectFit: 'contain' }}
            src={imageUrl}
            onError={(e) => e.target.src = `https://images-na.ssl-images-amazon.com/images/I/21u5Kw1n+IL._SX331_BO1,204,203,200_.jpg`}
            alt="Moive Poster"
          />
          <form className="gameform--form" onSubmit={(e) => submitHandler(e)}>
            <Grid container justify="center" spacing={2}>
              <Grid item xs={6}>
                <TextField
                  type="text"
                  label="Name"
                  variant="outlined"
                  fullWidth
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                  error={errMessage.name ? true : false}
                  helperText={errMessage.name}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  type="text"
                  label="Genre"
                  variant="outlined"
                  fullWidth
                  value={genre}
                  onChange={(e) => setGenre(e.target.value)}
                  error={errMessage.genre ? true : false}
                  helperText={errMessage.genre}
                />
              </Grid>
              <Grid item xs={4}>
                <TextField
                  type="text"
                  label="Release"
                  variant="outlined"
                  fullWidth
                  value={release}
                  onChange={(e) => setRelease(e.target.value)}
                  error={errMessage.release ? true : false}
                  helperText={errMessage.release}
                />
              </Grid>
              <Grid item xs={4}>
                <TextField
                  type="text"
                  label="Single Player"
                  variant="outlined"
                  fullWidth
                  value={singlePlayer}
                  onChange={(e) => setSinglePlayer(e.target.value)}
                  error={errMessage.singlePlayer ? true : false}
                  helperText={errMessage.singlePlayer}
                />
              </Grid>
              <Grid item xs={4}>
                <TextField
                  type="text"
                  label="Multiplayer"
                  variant="outlined"
                  fullWidth
                  value={multiplayer}
                  onChange={(e) => setMultiplayer(e.target.value)}
                  error={errMessage.multiplayer ? true : false}
                  helperText={errMessage.multiplayer}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  type="text"
                  label="Image Url"
                  variant="outlined"
                  fullWidth
                  value={imageUrl}
                  onChange={(e) => setImageUrl(e.target.value)}
                  error={errMessage.imageUrl ? true : false}
                  helperText={errMessage.imageUrl}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  type="text"
                  label="Platform"
                  variant="outlined"
                  fullWidth
                  value={platform}
                  onChange={(e) => setPlatform(e.target.value)}
                  error={errMessage.platform ? true : false}
                  helperText={errMessage.platform}
                  multiline
                  rows={4}
                />
              </Grid>
            </Grid>
            <Button
              variant="contained"
              color="primary"
              size="large"
              type="submit"
              fullWidth
            >
              Submit
            </Button>
          </form>
        </CardContent>
      </Card>
    </div>
  )
}

export default GameForm
