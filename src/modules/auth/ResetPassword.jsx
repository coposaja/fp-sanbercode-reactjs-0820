import React, { useState } from 'react';
import { TextField, Button, Card, CardContent } from '@material-ui/core';
import { withRouter } from 'react-router-dom';

import './ResetPassword.css';
import { customAxios } from '../../utils';

const ResetPassword = (props) => {
  const [currPassword, setCurrPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [confPassword, setConfPassword] = useState('');
  const [errMessage, setErrMessage] = useState({});

  const submitHandler = (e) => {
    e.preventDefault();
    if (!validateInput()) return;

    customAxios.post('change-password', {
      current_password: currPassword,
      new_password: newPassword,
      new_confirm_password: confPassword,
    })
      .then((res) => console.log(res))
      .catch((err) => console.log(JSON.parse(err.response.data)))
  }

  const validateInput = () => {
    let error = {};
    let flag = true;
    if (!currPassword) {
      error.currPassword = "Please input your current password"
      flag = false;
    }

    if (!newPassword) {
      error.newPassword = "Please input your new password"
      flag = false;
    }

    if (!confPassword) {
      error.confPassword = "Please input confirmation password"
      flag = false;
    }
    else if (confPassword !== newPassword) {
      error.confPassword = "Please re-input your new password"
      flag = false;
    }

    setErrMessage(error);
    return flag;
  }

  return (
    <div className="reset-password">
      <Card className="reset-password--card" variant="outlined">
        <CardContent>
          <h1 className='reset-password--title'>Reset Password</h1>
          <form onSubmit={(e) => submitHandler(e)}>
            <TextField
              type="password"
              label="Current Password"
              variant="outlined"
              className="reset-password--form-field"
              value={currPassword}
              onChange={(e) => setCurrPassword(e.target.value)}
              error={errMessage.currPassword ? true : false}
              helperText={errMessage.currPassword}
            />
            <TextField
              type="password"
              label="New Password"
              variant="outlined"
              className="reset-password--form-field"
              value={newPassword}
              onChange={(e) => setNewPassword(e.target.value)}
              error={errMessage.newPassword ? true : false}
              helperText={errMessage.newPassword}
            />
            <TextField
              type="password"
              label="Confirmation Password"
              variant="outlined"
              className="reset-password--form-field"
              value={confPassword}
              onChange={(e) => setConfPassword(e.target.value)}
              error={errMessage.confPassword ? true : false}
              helperText={errMessage.confPassword}
            />
            <Button
              variant="contained"
              color="primary"
              className="reset-password--form-field"
              size="large"
              type="submit"
            >
              Reset Password
            </Button>
          </form>
        </CardContent>
      </Card>
    </div>
  )
}

export default withRouter(ResetPassword);
