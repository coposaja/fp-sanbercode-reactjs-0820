import React, { useState, useContext } from 'react';
import { TextField, Button } from '@material-ui/core';
import Axios from 'axios';
import { withRouter } from 'react-router-dom';

import './Register.css';
import { validateEmail } from '../../utils';
import { AuthContext } from '../../contexts/AuthContext';

const Register = (props) => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [errMessage, setErrMessage] = useState({});
  const [, setUser] = useContext(AuthContext);

  const submitHandler = (e) => {
    e.preventDefault();
    if (!validateInput()) return;

    Axios.post('https://backendexample.sanbersy.com/api/register', {
      name: name,
      email: email,
      password: password,
    })
      .then((res) => {
        const { user, token } = res.data;
        const currentUser = { name: user.name, email: user.email, token };
        setUser(currentUser);
        localStorage.setItem("auth", JSON.stringify(currentUser));
        props.history.push('/');
      })
      .catch((err) => console.log(err))
  }

  const validateInput = () => {
    let error = {};
    let flag = true;
    if (!email) {
      error.email = "Please input your email"
      flag = false;
    }
    else if (!validateEmail(email)) {
      error.email = "Plaese input the right format"
      flag = false;
    }

    if (!password) {
      error.password = "Please input your password"
      flag = false;
    }

    if (!name) {
      error.name = "Please input your name"
      flag = false;
    }

    setErrMessage(error);
    return flag;
  }

  return (
    <div className="register">
      <h1 className='register--title'>Register</h1>
      <form onSubmit={(e) => submitHandler(e)}>
        <TextField
          type="text"
          label="Name"
          variant="outlined"
          className="register--form-field"
          value={name}
          onChange={(e) => setName(e.target.value)}
          error={errMessage.name ? true : false}
          helperText={errMessage.name}
        />
        <TextField
          type="email"
          label="Email"
          variant="outlined"
          className="register--form-field"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          error={errMessage.email ? true : false}
          helperText={errMessage.email}
        />
        <TextField
          type="password"
          label="Password"
          variant="outlined"
          className="register--form-field"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          error={errMessage.password ? true : false}
          helperText={errMessage.password}
        />
        <Button
          variant="contained"
          color="primary"
          className="register--form-field"
          size="large"
          type="submit"
        >
          Register
        </Button>
      </form>
    </div>
  )
}

export default withRouter(Register);
