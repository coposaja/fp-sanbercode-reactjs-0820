import { Button, TextField } from '@material-ui/core';
import Axios from 'axios';
import React, { useContext, useState } from 'react';
import { withRouter } from 'react-router-dom';

import './Login.css';
import { AuthContext } from '../../contexts/AuthContext';
import { validateEmail } from '../../utils';

const Login = (props) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [errMessage, setErrMessage] = useState({});
  const [, setUser] = useContext(AuthContext);

  const submitHandler = (e) => {
    e.preventDefault();
    if (!validateInput()) return;

    Axios.post('https://backendexample.sanbersy.com/api/user-login', {
      email: email,
      password: password,
    })
      .then((res) => {
        const { user, token } = res.data;
        const currentUser = { name: user.name, email: user.email, token };
        setUser(currentUser);
        localStorage.setItem("auth", JSON.stringify(currentUser));
        props.history.push('/movies');
      })
      .catch((err) => console.log(err))
  }

  const validateInput = () => {
    let error = {};
    let flag = true;
    if (!email) {
      error.email = "Please input your email"
      flag = false;
    }
    else if (!validateEmail(email)) {
      error.email = "Plaese input the right format"
      flag = false;
    }

    if (!password) {
      error.password = "Please input your password"
      flag = false;
    }

    setErrMessage(error);
    return flag;
  }

  return (
    <div className='login'>
      <h1 className='login--title'>Login</h1>
      <form onSubmit={(e) => submitHandler(e)}>
        <TextField
          type="email"
          label="Email"
          variant="outlined"
          className="login--form-field"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          error={errMessage.email ? true : false}
          helperText={errMessage.email}
        />
        <TextField
          type="password"
          label="Password"
          variant="outlined"
          className="login--form-field"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          error={errMessage.password ? true : false}
          helperText={errMessage.password}
        />
        <Button
          variant="contained"
          color="primary"
          className="login--form-field"
          size="large"
          type="submit"
        >
          Login
        </Button>
      </form>
    </div>
  )
}

export default withRouter(Login);
