import { Card, Tab, Tabs } from '@material-ui/core';
import React, { Component } from 'react';

import TabPanel from '../../components/TabPanel/TabPanel';
import Login from './Login';
import './Auth.css';
import Register from './Register';

class Auth extends Component {
  state = {
    activeTab: 0,
  };

  componentDidMount() {
    const user = localStorage.getItem('auth');
    if (user) this.props.history.push('/movies')
  }

  handleChange = (e, tabIndex) => {
    this.setState({ activeTab: tabIndex });
  }

  render() {
    const { activeTab } = this.state;
    return (
      <div className="auth">
        <Card className="auth--card" variant="outlined">
          <Tabs
            value={activeTab}
            indicatorColor="primary"
            textColor="primary"
            onChange={this.handleChange}
          >
            <Tab value={0} label="Login" />
            <Tab value={1} label="Register" />
          </Tabs>

          <TabPanel value={activeTab} index={0}>
            <Login />
          </TabPanel>
          <TabPanel value={activeTab} index={1}>
            <Register />
          </TabPanel>
        </Card>
      </div >
    )
  }
}

export default Auth;
