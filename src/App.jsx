import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import { AuthProvider } from './contexts/AuthContext';
import Layout from './components/Layout/Layout';
import Auth from './modules/auth/Auth';

const App = () => {
  return (
    <AuthProvider>
      <BrowserRouter>
        <Switch>
          <Route path="/auth" component={Auth} />
          <Route path="/" component={Layout} />
        </Switch>
      </BrowserRouter>
    </AuthProvider>
  );
}

export default App;
