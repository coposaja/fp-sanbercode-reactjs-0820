import {
  Button,
  IconButton,
  Toolbar,
  Tooltip,
  Typography
} from '@material-ui/core';
import {
  Delete as DeleteIcon,
  Edit as EditIcon
} from '@material-ui/icons';
import React from 'react'

const EnhancedTableToolbar = (props) => {
  const { selected, handleDelete, handleEdit, handleAdd, tableType } = props;

  return (
    <Toolbar style={{ padding: '0' }}>
      {selected.length > 0 ? (
        <Typography color="inherit" variant="subtitle1" component="div">
          {selected.length} selected
        </Typography>
      ) : (
          <Typography variant="h6" id="tableTitle" component="div">
            {tableType}
          </Typography>
        )}

      {selected.length > 0 ? (
        <>
          {selected.length === 1 ? (
            <Tooltip title="Edit">
              <IconButton aria-label="edit" onClick={() => handleEdit()}>
                <EditIcon />
              </IconButton>
            </Tooltip>
          ) : null}
          <Tooltip title="Delete">
            <IconButton aria-label="delete" onClick={() => handleDelete()}>
              <DeleteIcon />
            </IconButton>
          </Tooltip>
        </>
      ) : null}

      <Button
        variant="contained"
        color="secondary"
        size="large"
        style={{ position: 'absolute', right: '0' }}
        onClick={() => handleAdd()}
      >Add new {tableType}</Button>
    </Toolbar>
  );
};

export default EnhancedTableToolbar;
