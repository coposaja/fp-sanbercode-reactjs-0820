import React, { useContext, useEffect } from 'react';
import { AppBar, Container, Tab, Tabs, Toolbar } from '@material-ui/core';
import { Route, Switch } from 'react-router-dom';

import { AuthContext } from '../../contexts/AuthContext';
import MovieList from '../../modules/movies/MovieList';
import GameList from '../../modules/games/GameList';
import './Layout.css';
import MovieForm from '../../modules/movies/MovieForm';
import GameForm from '../../modules/games/GameForm';
import ResetPassword from '../../modules/auth/ResetPassword';

const Layout = (props) => {
  const [user] = useContext(AuthContext);

  useEffect(() => {
    if (!user) props.history.push('/auth');
  }, [user, props])

  const handleChange = (e, value) => {
    props.history.push(value);
  }

  const logoutHandler = () => {
    localStorage.removeItem('auth');
    props.history.push('/auth');
  }

  return (
    <div className="layout">
      <AppBar position="fixed" color="default">
        <Toolbar>
          <Tabs
            indicatorColor="primary"
            textColor="primary"
            onChange={handleChange}
            value={`/${props.history.location.pathname.split('/')[1]}`.toLowerCase()}
            style={{ width: '100%' }}
          >
            <Tab label="Movies" value="/movies" />
            <Tab label="Games" value="/games" />
            <Tab label="Reset Password" value="/reset-password" style={{ position: 'absolute', right: '160px' }} />
            <Tab label="Logout" onClick={() => logoutHandler()} style={{ position: 'absolute', right: '0' }} />
          </Tabs>
        </Toolbar>
      </AppBar>
      <Container className="layout--content">
        <Switch>
          <Route path="/movies/:id" component={MovieForm} />
          <Route path="/movies" component={MovieList} />
          <Route path="/games/:id" component={GameForm} />
          <Route path="/games" component={GameList} />
          <Route path="/reset-password" component={ResetPassword} />
        </Switch>
      </Container>
      <AppBar position="fixed" color="default" style={{ bottom: 0, top: 'auto' }}>
        <Toolbar style={{ display: 'flex', justifyContent: 'space-between' }}>
          <span>© Copyright by coposaja</span>
          <span>{new Date().getFullYear().toString()}</span>
        </Toolbar>
      </AppBar>
    </div>
  )
}

export default Layout;
