import Axios from "axios";

export const validateEmail = (email) => {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{1,}))$/;
  return re.test(email);
}

const user = JSON.parse(localStorage.getItem('auth'));
export const customAxios = Axios.create({
  baseURL: "https://backendexample.sanbersy.com/api/",
  headers: {
    "Authorization": `Bearer ${user ? user.token : ''}`
  }
});
